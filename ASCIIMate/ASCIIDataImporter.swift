//
//  ASCIIDataImporter.swift
//  ASCIIMate
//
//  Created by Dave Mountain on 21/10/2016.
//
//

import Cocoa

class ASCIIDataImporter {

    static func importFrom(fileNamed name: String, withExtension ext: String) -> [ASCIIObject]? {
        
        guard let path = Bundle.main.path(forResource: name, ofType: ext),
              let contents = try? String(contentsOfFile: path, encoding: .utf8) else {
            
            return nil
        }
        
        return deserialize(contents)
    }
    
    static func deserialize(_ data: String) -> [ASCIIObject] {
        
        return data.components(separatedBy: .newlines).flatMap {
            
            let columns = $0.components(separatedBy: "\t")
            
            if columns.count <= 1 {
                return nil
            }
            
            let asciiObject = ASCIIObject.create(forEditing: false)
            asciiObject.asciiString = columns.first!
            
            let phrases = columns.suffix(from: 1).map { str -> ASCIIKeyPhrase in
                let keyPhrase = ASCIIKeyPhrase.create()
                keyPhrase.phrase = str
                
                return keyPhrase
            }
            
            asciiObject.keyPhrases = NSSet(array: phrases)
            asciiObject.isFavourite = false
            
            return asciiObject
        }
    }
}
