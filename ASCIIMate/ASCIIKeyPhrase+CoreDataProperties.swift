//
//  ASCIIKeyPhrase+CoreDataProperties.swift
//  ASCIIMate
//
//  Created by Dave Mountain on 21/10/2016.
//
//

import Foundation
import CoreData

extension ASCIIKeyPhrase {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ASCIIKeyPhrase> {
        return NSFetchRequest<ASCIIKeyPhrase>(entityName: "ASCIIKeyPhrase");
    }

    @NSManaged public var phrase: String?
    @NSManaged public var asciiObject: ASCIIObject?
}
