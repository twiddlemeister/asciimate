//
//  NSViewController+RunLoop.swift
//  ASCIIMate
//
//  Created by Dave Mountain on 21/10/2016.
//
//

import Foundation
import Cocoa

extension NSViewController {
    
    // Will schedule block to run on next run loop iteration
    func onNextRunLoop(block: @escaping () -> Void) {
        
        AppUtils.delay(seconds: 0) {
            block()
        }
    }
}
