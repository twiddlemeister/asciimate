//
//  ASCIIKeyPhrase+CoreDataClass.swift
//  ASCIIMate
//
//  Created by Dave Mountain on 21/10/2016.
//
//

import Foundation
import CoreData

public class ASCIIKeyPhrase: NSManagedObject {
    
    class func create() -> ASCIIKeyPhrase {
        
        let context = ASCIIStore.shared.context
        
        let item = NSEntityDescription.insertNewObject(forEntityName: "ASCIIKeyPhrase", into: context) as! ASCIIKeyPhrase
        
        return item
    }
}
