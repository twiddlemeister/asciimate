//
//  ASCIIListItem.swift
//  ASCIIMate
//
//  Created by Dave Mountain on 20/10/2016.
//
//

import Foundation

protocol ASCIIListItem {
    
    var primaryText: String { get }
    var secondaryText: String { get }
}
