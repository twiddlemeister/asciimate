//
//  AppDelegate.swift
//  ASCIIMate
//
//  Created by David Mountain on 19/02/2016.
//
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject,
                   NSApplicationDelegate,
                   MainViewControllerDelegate,
                   KeyboardMonitorDelegate {
    
    var storyboard: NSStoryboard!
    var window: MainWindow!
    var windowController: NSWindowController!
    var keyboardMonitor: KeyboardMonitor!
    var statusItem: NSStatusItem!
    var openCloseItem: NSMenuItem!
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        
        acquirePrivileges()
        
        KeyboardMonitor.shared.delegate = self
        
        storyboard = NSStoryboard(name: "Main", bundle: Bundle.main)
        
        let mainController = storyboard.instantiateController(withIdentifier: "MainViewController") as! MainViewController
        mainController.delegate = self
        
        window = MainWindow(contentViewController: mainController)
        window.styleMask = NSWindowStyleMask.borderless
        
        windowController = NSWindowController(window: window)
        
        statusItem = NSStatusBar.system().statusItem(withLength: -1)
        statusItem.target = self
        statusItem.highlightMode = true
        statusItem.button?.toolTip = "ASCIIMate by @twiddlemeister"
        statusItem.button?.title = "ಠ_ಠ"
        statusItem.button?.font = NSFont(name: "System", size: 10)
        statusItem.menu = createMenu()
        
        ASCIIStore.shared.seedIfNeeded()
    }
    
    func createMenu() -> NSMenu {
        
        let menu = NSMenu(title: "ASCIIMate Menu")
        
        openCloseItem = NSMenuItem(title: "Show ASCIIMate", action: #selector(toggleWindowVisibility), keyEquivalent: "")
        openCloseItem.target = self
        openCloseItem.tag = 1003
        menu.addItem(openCloseItem)
        
        menu.addItem(NSMenuItem.separator())
        
        menu.addItem(withTitle: "ASCIIMate by @twiddlemeister", action: nil, keyEquivalent: "")
        
        return menu
    }
    
    func toggleWindowVisibility() {
        
        if windowController.window!.isVisible {
            
            print("Window exists, hiding")
            openCloseItem.title = "Show ASCIIMate"
            windowController.close()
            
        } else {
            
            print("Window is nil, showing")
            openCloseItem.title = "Hide ASCIIMate"
            windowController.showWindow(self)
        }
    }
    
    func keyboardMonitorDetectedMasterShortcut() {
        
        NSApplication.shared().activate(ignoringOtherApps: true)
        toggleWindowVisibility()
    }
    
    @discardableResult func acquirePrivileges() -> Bool {
        
        let options = [kAXTrustedCheckOptionPrompt.takeUnretainedValue() as String: true]
        
        let accessEnabled = AXIsProcessTrustedWithOptions(options as CFDictionary?)
        
        if accessEnabled == false {
            print("Can't get required privileges.")
        } else {
            print("App has required privileges. Listening for shortcut...")
        }
        
        return accessEnabled
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        
        ASCIIStore.shared.saveContext()
    }
    
    //
    // MARK: MainViewController delegate
    //
    
    func mainViewControllerDetectedEscapeKey() {
        
        toggleWindowVisibility()
        
        print("Detected escape key. Closed window.")
    }
    
    func mainViewController(_ mainViewController: MainViewController, requestsWindowSizeChangeTo size: NSSize) {
        
        var f = window.frame
        
        if size.height > f.size.height {
            f.origin.y += f.size.height - size.height
        } else {
            f.origin.y -= size.height - f.size.height
        }
        
        f.size = size
        
        window.setFrame(f, display: true, animate: true)
    }
}

