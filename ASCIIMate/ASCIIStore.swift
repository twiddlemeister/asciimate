//
//  ASCIIStore.swift
//  ASCIIMate
//
//  Created by David Mountain on 30/03/2016.
//
//

import Cocoa
import CoreData

class ASCIIStore {
    
    private struct Constants {
        static let databaseSeededKey = "databaseSeededKey"
    }
    
    static let shared = ASCIIStore()
    
    private let prefs: UserDefaults = UserDefaults.standard
    
    private init() {
        
    }
    
    //
    // MARK: Seeding
    //
    
    private func wipe() {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ASCIIObject")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try context.execute(deleteRequest)
        } catch {
            print("Couldn't wipe database: \(error)")
        }
    }
    
    func seedIfNeeded() {
        
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        
        if prefs.object(forKey: Constants.databaseSeededKey) == nil {
            
            // Ensure nothing is left over before seeding
            wipe()
            
            // This method is doing DB stuff and it probably shouldn't but fuck it for now
            _ = ASCIIDataImporter.importFrom(fileNamed: "seed", withExtension: "tsv")
            
            saveContext()
            
            prefs.set(true, forKey: Constants.databaseSeededKey)
        }
        
        printASCIIObjectsToConsole()
    }
    
    private func printASCIIObjectsToConsole() {
        
        print("\n===== ASCIIObjects: =====")
        
        ASCIIObject.getAll().forEach {
            
            let asciiObject = $0 as! ASCIIObject
            let phrases = asciiObject.keyPhrases as! Set<ASCIIKeyPhrase>
            let phraseString = phrases.map { $0.phrase! }.joined(separator: ", ")
            
            print("\(phraseString) (Used count: \(asciiObject.usedCount)) \(asciiObject.asciiString!)")
        }
        
        print("=========================")
    }
    
    //
    // MARK: Core Data
    //
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "ASCIIMate")
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        
        return container
    }()
    
    lazy var context: NSManagedObjectContext = {
        
        return self.persistentContainer.viewContext
    }()
    
    func saveContext() {
        
        let context = persistentContainer.viewContext
        
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
