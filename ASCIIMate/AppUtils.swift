//
//  AppUtils.swift
//  ASCIIMate
//
//  Created by David Mountain on 31/03/2016.
//
//

import Cocoa

extension NSColor {
    
    public convenience init?(hex: String) {
        
        let r, g, b, a: CGFloat
        
        if hex.hasPrefix("#") {
            
            let start = hex.characters.index(hex.startIndex, offsetBy: 1)
            let hexColor = hex.substring(from: start)
            
            if hexColor.characters.count == 8 {
                
                let scanner = Scanner(string: hexColor)
                
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    
                    return
                }
            }
        }
        
        return nil
    }
}

class AppUtils: NSObject {
    
    static func copyStringToPasteboard(_ str: String) {
        
        let pasteboard = NSPasteboard.general()
        pasteboard.clearContents()
        pasteboard.setString(str, forType: NSStringPboardType)
        
        print("Copied \"\(str)\" to clipboard")
    }
    
    static func delay(seconds: Double, block: @escaping () -> Void) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            block()
        }
    }
}
