//
//  ASCIITableHeaderView.swift
//  ASCIIMate
//
//  Created by Dave Mountain on 25/10/2016.
//
//

import Cocoa

class ASCIITableHeaderView: NSTableHeaderView {
    
    var textLabel: NSTextField!
    
    override init(frame frameRect: NSRect) {
        
        super.init(frame: frameRect)
        
        textLabel = NSTextField(labelWithString: "FAVOURITES")
        textLabel.textColor = NSColor.white
        
        var frame = textLabel.frame
        frame.origin.x = 20
        frame.origin.y = 2
        textLabel.frame = frame
        
        addSubview(textLabel)
    }
    
    required init?(coder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ dirtyRect: NSRect) {
        
        let color = NSColor(hex: "#ddddddff")!
        
        color.setFill()
        color.setStroke()
        
        let bezier = NSBezierPath(rect: self.bounds)
        
        bezier.fill()
        bezier.stroke()
    }
}
