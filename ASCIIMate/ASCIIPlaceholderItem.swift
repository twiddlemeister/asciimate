//
//  ASCIIPlaceholderItem.swift
//  ASCIIMate
//
//  Created by Dave Mountain on 20/10/2016.
//
//

import Cocoa

class ASCIIPlaceholderItem: ASCIIListItem {
    
    var primaryText: String {
        get { return "Add to dictionary?" }
    }
    
    var secondaryText: String {
        get { return "" }
    }
}
