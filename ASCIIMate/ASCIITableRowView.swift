//
//  ASCIITableRowView.swift
//  ASCIIMate
//
//  Created by David Mountain on 31/03/2016.
//
//

import Cocoa

class ASCIITableRowView: NSTableRowView {

    private struct Constants {
        static let cellSelectionColor = NSColor(hex: "#f9f9f9ff")!
        static let copyMessageColorSelected = NSColor(hex: "#3ace3aff")!
        static let copyMessageColorUnselected = NSColor(hex: "#ddddddff")!
        static let copiedMessageString = "✓ COPIED"
        static let savedMessageString = "✓ SAVED"
        static let copyMessageString = "⏎ TO COPY"
    }
    
    @IBOutlet weak var descriptionField: NSTextField!
    @IBOutlet weak var previewField: NSTextField!
    @IBOutlet weak var copyMessageField: NSTextField!
    
    override var isFlipped: Bool {
        return false
    }
    
    override var isSelected: Bool {
        didSet {
            copyMessageField.isHidden = !isSelected
        }
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        copyMessageField.textColor = Constants.copyMessageColorUnselected
        copyMessageField.stringValue = Constants.copyMessageString
    }
    
    override func prepareForReuse() {
        
        super.prepareForReuse()
        
        previewField.stringValue = ""
        previewField.textColor = NSColor.black
        
        descriptionField.stringValue = ""
        
        copyMessageField.stringValue = Constants.copyMessageString
        copyMessageField.textColor = Constants.copyMessageColorUnselected
    }
    
    override func drawSelection(in dirtyRect: NSRect) {
        
        fill(with: Constants.cellSelectionColor)
    }
    
    private func fill(with color: NSColor) {
        
        color.setFill()
        color.setStroke()
        
        let bezier = NSBezierPath(rect: self.bounds)
        
        bezier.fill()
        bezier.stroke()
    }
    
    func showCopiedMessage(block: @escaping () -> Void) {
        
        showStatusMessage(Constants.copiedMessageString, block: block)
    }
    
    func showSavedMessage(block: @escaping () -> Void) {
        
        showStatusMessage(Constants.savedMessageString, block: block)
    }
    
    private func showStatusMessage(_ message: String, block: @escaping () -> Void) {
        
        copyMessageField.textColor = Constants.copyMessageColorSelected
        copyMessageField.stringValue = message
        
        AppUtils.delay(seconds: 1) {
            block()
        }
    }
}
