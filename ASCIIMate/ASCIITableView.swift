//
//  ASCIITableView.swift
//  ASCIIMate
//
//  Created by David Mountain on 31/03/2016.
//
//

import Cocoa

protocol ASCIITableViewDelegate {
    func tableView(_ tableView: ASCIITableView, didDetectClickOnRow row: Int)
}

class ASCIITableView: NSTableView {

    var extendedDelegate: ASCIITableViewDelegate?
    
    override func mouseDown(with theEvent: NSEvent) {
        
        let loc = convert(theEvent.locationInWindow, from: nil)
        let row = self.row(at: loc)
        
        super.mouseDown(with: theEvent)
        
        if row > -1 {
            extendedDelegate?.tableView(self, didDetectClickOnRow: row)
        }
    }
    
    func selectedRowView() -> ASCIITableRowView {
        
        return rowView(atRow: selectedRow, makeIfNecessary: false) as! ASCIITableRowView
    }
}
