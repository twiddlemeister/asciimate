//
//  MainViewController.swift
//  ASCIIMate
//
//  Created by David Mountain on 30/03/2016.
//
//

import Cocoa

protocol MainViewControllerDelegate {
    func mainViewControllerDetectedEscapeKey()
    func mainViewController(_ mainViewController: MainViewController, requestsWindowSizeChangeTo size: NSSize)
}

class MainViewController: NSViewController, NSTextFieldDelegate, NSTableViewDataSource, NSTableViewDelegate, ASCIITableViewDelegate {

    private struct Constants {
        static let cellIdentifier = "ASCIITableRowView"
    }
    
    @IBOutlet weak var searchField: NSTextField!
    @IBOutlet weak var resultsTableView: ASCIITableView!
    
    var delegate: MainViewControllerDelegate?
    var items: [ASCIIListItem]?
    var expanded = false
    var editMode = false
    var editingObject: ASCIIObject?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        searchField.delegate = self
        
        let nib = NSNib(nibNamed: Constants.cellIdentifier, bundle: Bundle.main)
        resultsTableView.register(nib, forIdentifier: Constants.cellIdentifier)
        
        resultsTableView.selectionHighlightStyle = NSTableViewSelectionHighlightStyle.regular
        
        resultsTableView.dataSource = self
        resultsTableView.delegate = self
        resultsTableView.extendedDelegate = self
        
        reloadData(forSearchTerm: "")
    }
    
    //
    // MARK: NSControl delegate methods
    //
    
    override func controlTextDidChange(_ obj: Notification) {
        
        if !editMode {
            reloadData(forSearchTerm: searchField.stringValue)
        }
    }
    
    func control(_ control: NSControl, textView: NSTextView, doCommandBy commandSelector: Selector) -> Bool {
        
        switch commandSelector.description {
            
            case "moveUp:", "moveDown:":
                handleDirectionalKeys()
                return true
            
            case "insertNewline:":
                handleReturnKey(control)
                break
            
            case "cancelOperation:":
                handleEscapeKey()
                break
            
            default:
                break
        }
        
        return false
    }
    
    //
    // MARK: Key down handlers
    //
    
    private func handleDirectionalKeys() {
        
        resultsTableView.keyDown(with: NSApp.currentEvent!)
    }
    
    private func handleReturnKey(_ control: NSControl) {
        
        let item = items![resultsTableView.selectedRow]
        let isPlaceholderItem = item is ASCIIPlaceholderItem
        let isMainSearchField = control.isEqual(searchField)
        
        if editMode && !isMainSearchField {
            
            // User has created a new ASCIIObject
            let field = control as! NSTextField
            switchToSearchMode(savingContentsOf: field)
            
            return
            
        } else if isPlaceholderItem && isMainSearchField {
            
            // User has begun to create a new ASCIIObject
            switchToEditMode()
            return
            
        } else {
            
            // User wants to copy ASCIIObject
            let asciiObject = item as! ASCIIObject
            asciiObject.usedCount += 1
            
            ASCIIStore.shared.saveContext()
            
            copyAsciiToClipboard(forIndex: resultsTableView.selectedRow)
            
            resultsTableView.selectedRowView().showCopiedMessage {
                self.reset()
            }
        }
    }
    
    private func handleEscapeKey() {
        
        delegate?.mainViewControllerDetectedEscapeKey()
    }
    
    //
    // MARK: App state changes
    //
    
    private func switchToSearchMode(savingContentsOf field: NSTextField) {
        
        editMode = false
        
        field.delegate = nil
        
        editingObject!.asciiString = field.stringValue
        editingObject!.includeInResults = true
        editingObject!.usedCount += 1
        
        ASCIIStore.shared.saveContext()
        
        copyAsciiToClipboard(from: editingObject!)
        
        resultsTableView.selectedRowView().showSavedMessage {
            self.reset()
        }
    }
    
    private func switchToEditMode() {
        
        editMode = true
        
        editingObject = ASCIIObject.create()
        
        let keyPhrase = ASCIIKeyPhrase.create()
        keyPhrase.phrase = searchField.stringValue
        editingObject!.addToKeyPhrases(keyPhrase)
        
        reloadData(forSearchTerm: searchField.stringValue)
    }
    
    /*
    private func expandWindow() {
        
        expanded = true
        delegate?.mainViewController(self, requestsWindowSizeChangeTo: Constants.expandedWindowSize)
    }
    
    private func contractWindow() {
        
        expanded = false
        delegate?.mainViewController(self, requestsWindowSizeChangeTo: Constants.contractedWindowSize)
    }
    */
    
    //
    // MARK: Data loading
    //
    
    private func reloadData(forSearchTerm term: String) {
        
        guard term.characters.count > 3 else {
            reset()
            return
        }
        
        items = ASCIIObject.getAll(matching: term)
        
        guard items != nil else {
            reset()
            return
        }
        
        resultsTableView.headerView = nil
        resultsTableView.reloadData()
        resultsTableView.selectRowIndexes(IndexSet(integer: 0), byExtendingSelection: false)
        
        /*if !expanded {
            expandWindow()
        }*/
    }
    
    private func reset() {
        
        items = ASCIIObject.getMostUsed()
        
        resultsTableView.headerView = ASCIITableHeaderView()
        resultsTableView.reloadData()
        
        editingObject = nil
        
        /*if expanded {
            contractWindow()
        }*/
    }
    
    //
    // MARK: Pasteboard
    //
    
    private func copyAsciiToClipboard(forIndex index: Int) {
        
        guard index > -1 else { return }
        
        let item = items![index]
        
        AppUtils.copyStringToPasteboard(item.secondaryText)
        
        searchField.stringValue = ""
    }
    
    private func copyAsciiToClipboard(from object: ASCIIObject) {
        
        AppUtils.copyStringToPasteboard(object.asciiString!)
        
        searchField.stringValue = ""
    }
    
    //
    // MARK: NSTableViewDelegate, NSTableViewDataSource, and ASCIITableView delegate
    //
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        
        guard let items = items else {
            return 0
        }
        
        return items.count
    }
    
    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: NSTableView, rowViewForRow row: Int) -> NSTableRowView? {
        
        let cell = tableView.make(withIdentifier: Constants.cellIdentifier, owner: self) as! ASCIITableRowView
        
        let item = items![row]
        
        if item is ASCIIPlaceholderItem && editMode {
            
            cell.previewField.isEditable = true
            cell.previewField.stringValue = ""
            
            cell.descriptionField.stringValue = searchField.stringValue
            
            onNextRunLoop {
                NSApp.keyWindow?.makeFirstResponder(cell.previewField)
                cell.previewField.delegate = self
            }
            
        } else {
            
            cell.descriptionField.stringValue = item.primaryText
            cell.previewField.stringValue = item.secondaryText
        }
        
        return cell
    }
    
    func tableView(_ tableView: ASCIITableView, didDetectClickOnRow row: Int) {
        
        let item = items![row]
        
        if item is ASCIIPlaceholderItem {
            switchToEditMode()
            return
        }
        
        copyAsciiToClipboard(forIndex: row)
        
        reset()
    }
}
