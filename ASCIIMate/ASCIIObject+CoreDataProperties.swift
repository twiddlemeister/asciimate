//
//  ASCIIObject+CoreDataProperties.swift
//  ASCIIMate
//
//  Created by Dave Mountain on 18/10/2016.
//
//

import Foundation
import CoreData

extension ASCIIObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ASCIIObject> {
        return NSFetchRequest<ASCIIObject>(entityName: "ASCIIObject");
    }

    @NSManaged public var asciiString: String?
    @NSManaged public var includeInResults: Bool
    @NSManaged public var isFavourite: Bool
    @NSManaged public var usedCount: Int32
    @NSManaged public var keyPhrases: NSSet?
}

extension ASCIIObject {
    
    @objc(addKeyPhrasesObject:)
    @NSManaged public func addToKeyPhrases(_ value: ASCIIKeyPhrase)
    
    @objc(removeKeyPhrasesObject:)
    @NSManaged public func removeFromKeyPhrases(_ value: ASCIIKeyPhrase)
    
    @objc(addKeyPhrases:)
    @NSManaged public func addToKeyPhrases(_ values: NSSet)
    
    @objc(removeKeyPhrases:)
    @NSManaged public func removeFromKeyPhrases(_ values: NSSet)
}
