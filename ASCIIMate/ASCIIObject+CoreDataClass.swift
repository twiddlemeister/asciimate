//
//  ASCIIObject+CoreDataClass.swift
//  ASCIIMate
//
//  Created by Dave Mountain on 18/10/2016.
//
//

import Foundation
import CoreData

@objc(ASCIIObject)
public class ASCIIObject: NSManagedObject, ASCIIListItem {
    
    //
    // MARK: ASCIIListItem
    //
    
    var primaryText: String {
        get {
            
            // ASCIIObject can have more than one key phrase associated with it, so
            // using the last search string here so we can show the user the most
            // appropriate one in the results
            //
            // DISCLAIMER: There is *always* a better way.
            
            let keyPhrase = closestKeyPhrase(to: ASCIIObject.lastSearchString) ?? getKeyPhraseSet().first
            return keyPhrase != nil ? keyPhrase!.phrase! : ""
        }
    }
    
    var secondaryText: String {
        get { return self.asciiString! }
    }
    
    //
    // MARK: ASCIIObject
    //
    
    private struct Constants {
        static let favouritesFetchLimit = 3
    }
    
    static var lastSearchString: String? = ""
    
    func getKeyPhraseSet() -> Set<ASCIIKeyPhrase> {
        
        return keyPhrases as! Set<ASCIIKeyPhrase>
    }
    
    func closestKeyPhrase(to string: String?) -> ASCIIKeyPhrase? {
        
        guard let string = string else {
            return nil
        }
        
        return getKeyPhraseSet().first {
            return $0.phrase!.hasPrefix(string)
        }
    }
    
    class func getAll() -> [ASCIIListItem] {
        
        return getAll(matching: nil)
    }
    
    class func getAll(matching string: String?) -> [ASCIIListItem] {
        
        ASCIIObject.lastSearchString = string
        
        let request: NSFetchRequest<ASCIIObject> = ASCIIObject.fetchRequest()
        
        var predicates = [
            NSPredicate(format: "includeInResults = true")
        ]
        
        if let filterString = string {
            predicates.append(NSPredicate(format: "ANY keyPhrases.phrase BEGINSWITH[c] %@", filterString))
        }
        
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        
        guard let matches = try? ASCIIStore.shared.context.fetch(request) else {
            print("Fetch failed for some reason.")
            return []
        }
        
        if matches.count == 0 {
            return [ASCIIPlaceholderItem()]
        }
        
        return matches
    }
    
    class func getMostUsed() -> [ASCIIListItem] {
        
        let request: NSFetchRequest<ASCIIObject> = ASCIIObject.fetchRequest()
        request.fetchLimit = Constants.favouritesFetchLimit;
        
        let predicates = [
            NSPredicate(format: "includeInResults = true"),
            NSPredicate(format: "usedCount > 0")
        ]
        
        let sortDescriptors = [
            NSSortDescriptor(key: "usedCount", ascending: false)
        ]
        
        request.sortDescriptors = sortDescriptors
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        
        guard let matches = try? ASCIIStore.shared.context.fetch(request) else {
            print("Fetch failed for some reason.")
            return []
        }
        
        return matches
    }
    
    class func create(forEditing editing: Bool = true) -> ASCIIObject {
        
        let context = ASCIIStore.shared.context
        
        let item = NSEntityDescription.insertNewObject(forEntityName: "ASCIIObject", into: context) as! ASCIIObject
        item.includeInResults = !editing
        item.usedCount = 0
        
        if editing {
            item.asciiString = ""
            item.isFavourite = false
        }
        
        return item
    }
}
