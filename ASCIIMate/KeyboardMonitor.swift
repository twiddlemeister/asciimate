//
//  KeyboardMonitor.swift
//  ASCIIMate
//
//  Created by Dave Mountain on 14/10/2016.
//
//

import Cocoa

protocol KeyboardMonitorDelegate: class {
    func keyboardMonitorDetectedMasterShortcut()
}

class KeyboardMonitor {

    static let shared: KeyboardMonitor = KeyboardMonitor()
    
    weak var delegate: KeyboardMonitorDelegate?
    var globalEventMonitor: Any!
    var localEventMonitor: Any!
    var isShiftDown = false
    
    private init() {
        
        globalEventMonitor = NSEvent.addGlobalMonitorForEvents(matching: .keyDown, handler: handleGlobalKeyEvent)
        localEventMonitor = NSEvent.addLocalMonitorForEvents(matching: .flagsChanged, handler: handleLocalKeyEvent)
    }
    
    func handleGlobalKeyEvent(_ event: NSEvent) -> Void {
        
        let modifiers = event.modifierFlags
        
        checkForMasterShortcut(using: modifiers, and: event)
    }
    
    func handleLocalKeyEvent(_ event: NSEvent) -> NSEvent {
        
        let modifiers = event.modifierFlags
        
        checkForFlagChange(using: modifiers)
        
        return event
    }
    
    func checkForMasterShortcut(using modifiers: NSEventModifierFlags, and event: NSEvent) {
        
        let openModifiers: NSEventModifierFlags = [.control, .shift]
        
        if modifiers.contains(openModifiers) && event.keyCode == 49 {
            print("Master shortcut recognised.")
            delegate?.keyboardMonitorDetectedMasterShortcut()
        }
    }
    
    func checkForFlagChange(using modifiers: NSEventModifierFlags) {
        
        isShiftDown = modifiers.contains(.shift)
    }
}
