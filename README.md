# ASCIIMate #

Express yourself using ASCII art quickly and easily via a "Spotlight-esque" search tool.

## How to use ##

* Ctrl + Shift + Space to open the search box
* Start typing for what you want (for example "lenny face")
* Hit enter to copy to your clipboard
* Your search isn't in the database? Hit enter when prompted, paste in your ASCII and hit enter again to save for next time!